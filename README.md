# l3_projet_exemple

Example source files.

## setup

```sh
# clone project
git clone https://gitlab.com/l3_projet_2020/l3_projet_exemple.git

# install
cd l3_projet_exemple/
npm install

# serve at 'localhost:3000'
npm run devstart
```

## unit tests

```sh
# install
cd test/
npm install

# run
npm run test
```